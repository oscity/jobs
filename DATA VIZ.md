# D3 - Índice de Desarrollo Humano

Desplegar una gráfica de barras en la que se muestran los valores del Índice de Desarrollo Humano de las 32 entidades de México.

En la parte superior se ubican 3 dropdowns que permiten:

1. Seleccionar un estado
2. Seleccionar un año
3. Ordenar los datos (alfabéticamente (A - Z) | ascendente (0 - 1) | descendente (1 - 0))

## Datos

Generar aleatoriamente los datos para las gráficas en cuanto se carga la página

1. Elegir un rango aleatorio de años (más de 5)
2. Cada año deberá tener datos para las 32 entidades
3. Dar a cada entidad y año un valor aleatorio al IDH entre 0 y 1 (usar decimales)

## Gráfica

Encapsular la lógica para dibujar la gráfica en un componente de Vue que haga uso de los siguientes atributos:

1. data: un arreglo de objetos con los datos con los que se pintará la gráfica
2. selectedYear: año que se quiere visualizar
3. selectedState: Estado seleccionado -> destacar la barra de este estado con un color distinto
4. sort: forma de ordenar los datos

## UX

Cuando el ancho de la pantalla sea mayor a 480px, las barras deben ser verticales y los rótulos deben mostrar el nombre completo del estado; mientras que cuando sea menor o igual a 480px, las barras deben ser horizontales y los rótulos deben mostrar la abreviatura del estado.

Hacer la interfaz responsiva utilizando Vuetify.

La gráfica debe redibujarse cuando cambie el estado seleccionado, el año o la forma de ordenamiento, sin necesidad de botones adicionales.

## Tech

1. Seguir la guía de estilo del framework correspondiente - Vue 
2. Instalar dependencias usando yarn
3. Comentar y explicar el código
4. Implementar TDD - Unit testing con Jest + End to End con Cypres 

## Entrega

Compartir el repositorio de Git en el que esté el código y si es posible, subir el proyecto usando Firebase hosting.

## Mockup

![mockupD3](assets/mockupD3.jpg)
