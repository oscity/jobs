# Índice de Desarrollo Humano

Despliega una tabla utilizando que muestre los valores del Índice de Desarrollo Humano de las 32 entidades de México.

En la parte superior ubica 3 dropdowns que permitan:

1. Seleccionar un estado.
2. Seleccionar un o más años.
3. Ordenar los datos (alfabéticamente (A - Z) | ascendente (0 - 1) | descendente (1 - 0)).

## Tech

1. Utiliza **Nuxt.js / Next.js** como el framework para el desarrollo del proyecto.
2. Sigue la guía de estilo de **Nuxt.js / Next.js** y **Vue.js / React** para mantener una estructura de código consistente.
3. Utiliza **Vuetify / MUI** como framework de front-end.
5. Instala las dependencias utilizando yarn.
6. Comenta y explica el código.
7. Implementa TDD: Pruebas end-to-end con **Cypress**. (Minimo 3)

## Datos

Genera y administra los datos:

1. Genera aleatoriamente los datos para la tabla en cuanto se carga la página. (**más de 5 años**)
2. Asigna a cada entidad y año un valor aleatorio para el Índice de Desarrollo Humano (IDH) entre 0 y 1 (utiliza decimales).
3. Implementa un formulario para crear nuevos años.
4. Implementa opereaciones de lectura, actualización y eliminación para años.
5. Cada año debe tener datos para las 32 entidades.

## Tabla

Utiliza el componente **simple table** y **pagination** para realizar lo siguiente:

1. data: un arreglo de objetos con los datos que se mostraran en la tabla.
2. selectedYear: el año que se desea visualizar.
3. selectedState: los estados seleccionados.
4. sort: la forma de ordenar los datos.
5. pagination: pagina los datos.

## UX
Implementa la interfaz responsiva utilizando los breakpoints de **Vuetify / MUI**. (xs, sm, md, lg, xl)

## Entrega

Comparte el repositorio de Git que contiene el código y sube el proyecto utilizando **Firebase Hosting**.

## ¡Buena suerte!
